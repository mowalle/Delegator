#include <iostream>

class CopyTester {
public:	
	int id;
	int value;
	std::string status;

	CopyTester()
	    : id{counter++}
	    , value{}
	    , status{std::to_string(id) + ": Default constructed"}
	{ printStatus(); }

	explicit CopyTester(int value)
	    : id{counter++}
	    , value{value}
	    , status{std::to_string(id) + ": Value constructed"}
	{ printStatus(); }

	CopyTester(const CopyTester& other)
	    : id{counter++}
	    , value{other.value}
	    , status{std::to_string(id) + ": Copy constructed from ID: " + std::to_string(other.id)}
	{ printStatus(); }

	CopyTester(CopyTester&& other)
	    : id{std::move(other.id)}
	    , value{std::move(other.value)}
	    , status{std::to_string(id) + ": Move constructed from ID: " + std::to_string(id)}
	{ printStatus(); }

	~CopyTester()
	{
		std::cout << id << ": Destructor" << std::endl;
	}

	CopyTester& operator=(const CopyTester& other)
	{
		value = other.value;
		status = std::to_string(id) + ": Copy assigned from " + std::to_string(other.id);
		printStatus();
		return *this;
	}

	CopyTester& operator=(CopyTester&& other)
	{
		int oldId = id;
		id = std::move(other.id);
		value = std::move(other.value);
		status = std::to_string(oldId) + "->" + std::to_string(id) + ": Move assigned from " + std::to_string(id);
		printStatus();
		return *this;
	}

	void printStatus () const
	{ std::cout << status << std::endl; }

private:
	static int counter;
};

int CopyTester::counter = 0;
