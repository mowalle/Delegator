#include "Delegator.hxx"
#include "CopyTester.hxx"

#include <iostream>
#include <string>

struct TestStruct {
	void myTestFunc(int i, const std::string& s, bool b) const
	{
		std::cout << "\nTestFunc::myTestFunc:\nMy integer is " << i
		          << "\nMy string is " << s
		          << "\nMy bool is " << std::string(b ? "true" : "false") << '\n';
	}

	static void myStaticTestFunc(int i, const std::string& s, bool b)
	{
		std::cout << "\nTestStruct::myStaticTestFunc:\nMy integer is " << i
		          << "\nMy string is " << s
		          << "\nMy bool is " << std::string(b ? "true" : "false") << '\n';
	}
};

void globalFunc(int i, const std::string& s, bool b)
{
	std::cout << "\nglobalFunc:\nMy integer is " << i
	          << "\nMy string is " << s
	          << "\nMy bool is " << std::string(b ? "true" : "false") << '\n';
}

void process(CopyTester ct) {}
void processRef(CopyTester& ct) {}
void processConstRef(const CopyTester& ct) {}

int main()
{
	Delegator<int, const std::string&, bool> OnNewData;

	// Add free function.
	OnNewData.addDelegate(globalFunc);

	// Add static function.
	OnNewData.addDelegate(TestStruct::myStaticTestFunc);

	TestStruct myStruct;
	// Add member function via lambda...
	OnNewData.addDelegate([=](int i, const std::string& s, bool b) { myStruct.myTestFunc(i, s, b); });
	// ... or via bind with placeholders.
	OnNewData.addDelegate(std::bind(&TestStruct::myTestFunc, &myStruct, std::placeholders::_1, std::placeholders::_2, std::placeholders::_3));

	// ... do work ...

	OnNewData.broadcast(42, "String", false);

	/////////////////////////////////////////

	Delegator<CopyTester> dValue;
	Delegator<CopyTester&> dRef;
	Delegator<const CopyTester&> dConstRef;

	// Trying all combinations...

	dValue.addDelegate(process);         // one copy, one move
	// dValue.addDelegate(processRef);   // error: cannot convert CopyTester to CopyTester&.
	dValue.addDelegate(processConstRef); // one copy

	dRef.addDelegate(process);         // one copy
	dRef.addDelegate(processRef);
	dRef.addDelegate(processConstRef);

	dConstRef.addDelegate(process);         // one copy
	// dConstRef.addDelegate(processRef);   // error: cannot convert const CopyTester& to CopyTester&.
	dConstRef.addDelegate(processConstRef);

	CopyTester ct{22};
	dValue.broadcast(ct);
	dRef.broadcast(ct);
	dConstRef.broadcast(ct);

	return EXIT_SUCCESS;
}
