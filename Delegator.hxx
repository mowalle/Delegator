/// Delegator class inspired by Unreal Engine's delegator macros.
/// This class allows function delegates with a variable number of parameters.

#ifndef GUARD_DELEGATOR_HXX
#define GUARD_DELEGATOR_HXX

#include <functional>
#include <vector>

/// Delegator class.
/// The template types will correspond to the parameters of the delegates.
/// Both delegates with no parameters and a variable number of parameters are supported.
template <typename... ParamTypes>
class Delegator
{
public:
	/// Adds a delegate function (free function, static/member function, lambda, ...) to this Delegator.
	/// The function's parameter types must correspond to the delegator's template types.
	///
	/// \brief addDelegate Add delegate function to this delegator.
	/// \param delegate The function to add.
	///
	void addDelegate(const std::function<void(ParamTypes... params)>& delegate)
	{
		delegates.push_back(delegate);
	}

	/// Execute the list of delegates with the parameters passed to this function.
	///
	/// \brief broadcast Execute list of delegates.
	/// \param params Parameters passed to all delegates.
	///
	void broadcast(const ParamTypes&... params)
	{
		for (const auto& delegate : delegates) {
			delegate(params...);
		}
	}

private:
	std::vector<std::function<void(const ParamTypes&... params)>> delegates;
};

#endif // GUARD_DELEGATOR_HXX
